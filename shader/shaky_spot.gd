tool

extends SpotLight

export (bool) var play:bool = false setget do_play

var pos = null

func do_play( b:bool ):
	play = b
	pos = null

func _ready():
	pass # Replace with function body.

func _process(delta):

	if pos == null:
		pos = translation
	if play:
		transform.basis *= Basis( Vector3.FORWARD, delta )
		pass
#		translation = pos + transform.basis.z * rand_range(-0.001,0.001)
